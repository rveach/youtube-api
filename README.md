# youtube-api

This is a simple collection of methods to call the youtube api.
Coded for python 3 using the api key and requests.

Methods are added as I have a need in my private projects.
Contributions are welcome.

## Examples:

### Initialize:
Accepts the api key as the only parameter.
```python
import rvyoutube
youtube = rvyoutube.youtube("<your-api-key-here>")
```

### Get Playlist Items:
Accepts the playlist id as the only required parameter.

[API Reference](https://developers.google.com/youtube/v3/docs/playlistItems/list)

```python
status_code, playlist_data = youtube.get_playlist_items("<playlist-id-here>")
```

Please see code for all options.