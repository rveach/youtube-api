from setuptools import setup
setup(
  name = 'rvyoutube',
  packages = ['rvyoutube'],
  version = '0.1.2',
  description = 'simple collection of methods to call the youtube api.',
  author = 'Ryan Veach',
  author_email = 'rveach@gmail.com',
  url = 'https://gitlab.com/rveach/youtube-api',
  download_url = 'https://gitlab.com/rveach/youtube-api/repository/master/archive.tar.gz',
  keywords = ['YouTube', 'Google'],
  classifiers = [],
  install_requires=['requests'],
)