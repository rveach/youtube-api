import requests

def thumbnailurl(videoid, resolution="default"):

	resolution = resolution.lower()
	
	if resolution not in ['default', 'high', 'maxres', 'medium', 'standard']:
		resolution = "default"

	url = "https://i.ytimg.com/vi/%s/%s.jpg" % (videoid, resolution)
	
	return url


class youtube:

	# accepts the playlist id and returns a dictionary object
	# will return id's for deleted and private videos, but this can be suppressed
	def get_playlist_items(self, playlist_id,
		part="contentDetails,id,snippet,status",
		maxresults=50,
		debug=False,
		endpoint = "https://www.googleapis.com/youtube/v3/playlistItems",
		raise_for_status = True,
		only_public = False
	):
		retrieved_videos = []
		runloop = True
		pageToken = None

		# loop will loop as long as there is a next page token in the response.
		while runloop:

			# make the api call
			status_code, playlist_data = self.__get_playlist_items_call(
				playlist_id, pageToken=pageToken)

			# check for nextpagetoken, set up next loop
			if 'nextPageToken' in playlist_data.keys():
				# set the token for the next api call
				pageToken = playlist_data['nextPageToken']
			else:
				# break loop on next run
				runloop = False

			# iterate over items in response.
			# check for known issues where we can't download anymore.
			for v in playlist_data['items']:

				if 'videoPublishedAt'  not in v['contentDetails'].keys() and v['snippet']['title'].lower() == "deleted video":
					# this is a deleted video
					this_vid = {
						"status": "deleted",
						"videoId": v['contentDetails']['videoId'],
						"title": "Deleted Video",
					}
					if not only_public:
						retrieved_videos.append(this_vid)

				elif v['status']['privacyStatus'].lower() == "private":
					# this is a private video, we cannot download it
					this_vid = {
						"status": "private",
						"videoId": v['contentDetails']['videoId'],
						"title": "Private Video",
					}
					if not only_public:
						retrieved_videos.append(this_vid)

				else:
					# this video is *probably* good
					this_vid = {
						"status": v['status']['privacyStatus'],
						"videoId": v['contentDetails']['videoId'],
						"title": v['snippet']['title'],
					}

					# check for publish date on all vids
					if 'videoPublishedAt' in v['contentDetails'].keys():
						this_vid['videoPublishedAt'] = v['contentDetails']['videoPublishedAt']

					# append good vids
					retrieved_videos.append(this_vid)

		return retrieved_videos		


	# private call to make the raw requests call.
	# accepts the playlist id as the only required parameter
	# returns the status code and data
	def __get_playlist_items_call(self, playlist_id,
		part="contentDetails,id,snippet,status",
		maxresults=50,
		debug=False,
		endpoint = "https://www.googleapis.com/youtube/v3/playlistItems",
		raise_for_status = True,
		pageToken=None,
	):

		payload = {
			"playlistId": playlist_id,
			"maxResults": maxresults,
			"part": part,
			"key": self.api_key,
		}

		# add the page token
		if pageToken:
			payload['pageToken'] = pageToken

		r = requests.get(endpoint, params=payload)

		if debug:
			print("URL: %s" % r.url)
			print("Status Code: %s" % r.status_code)

		if raise_for_status:
			r.raise_for_status()

		return r.status_code, r.json()


	def __init__(self, api_key):
		self.api_key = api_key
